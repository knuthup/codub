set(PLUGIN_NAME nvstreammuxers)

find_package(GStreamer COMPONENTS base video REQUIRED)
find_package(DeepStream REQUIRED)

set(PLUGIN_srcs
    plugin.cpp
    gst_nv_auto_stream_mux.cpp
)

add_library(${PLUGIN_NAME} SHARED ${PLUGIN_srcs})

target_include_directories(${PLUGIN_NAME}
    PUBLIC GStreamer::gstreamer
    PRIVATE DeepStream::DeepStream
)

target_link_libraries(${PLUGIN_NAME}
    PUBLIC GStreamer::gstreamer
    PUBLIC DeepStream::DeepStream
)

target_compile_definitions(${PLUGIN_NAME} PRIVATE PLUGIN_NAME=${PLUGIN_NAME})

install(
    TARGETS ${PLUGIN_NAME}
    RUNTIME DESTINATION ${GStreamer_PLUGINS_DIR} COMPONENT shlib
    LIBRARY DESTINATION ${GStreamer_PLUGINS_DIR} COMPONENT shlib
)
