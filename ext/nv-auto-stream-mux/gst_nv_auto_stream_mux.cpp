/*
# Copyright (c) 2020, Andrew Knuthup
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SECTION:element-nvautostreammux
 *
 * Mux Batch Metadata into the stream without specifying dimensions
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v videotestsrc ! nvvideoconvert ! nvautostreammux ! nvinfer config-file-path=config_infer_primary_yoloV3.txt batch-size=1 unique-id=1 ! nvvideoconvert ! nvdsosd ! nvegltransform ! nveglglessink sync=false
 * ]|
 * </refsect2>
 */

#include "gst_nv_auto_stream_mux.h"

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gstnvdsmeta.h>

#define GST_CAT_DEFAULT gst_nv_auto_stream_mux_debug

enum
{
    PROP_0
};

#define GST_CAPS_FEATURE_MEMORY_NVMM "memory:NVMM"
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE(
    "sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(
        GST_VIDEO_CAPS_MAKE_WITH_FEATURES(
            GST_CAPS_FEATURE_MEMORY_NVMM,
            "{ NV12, RGBA }"
        )
    )
);

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE(
    "src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(
        GST_VIDEO_CAPS_MAKE_WITH_FEATURES(
            GST_CAPS_FEATURE_MEMORY_NVMM,
            "{ NV12, RGBA }"
        )
    )
);

#define _do_init \
    GST_DEBUG_CATEGORY_INIT(gst_nv_auto_stream_mux_debug, "nvautostreammux", 0, "Nv Auto Stream Mux");
#define gst_nv_auto_stream_mux_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE(GstNvAutoStreamMux, gst_nv_auto_stream_mux, GST_TYPE_ELEMENT, _do_init)

static void gst_nv_auto_stream_mux_finalize(GObject* object);
static void gst_nv_auto_stream_mux_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec);
static void gst_nv_auto_stream_mux_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec);
static gboolean gst_nv_auto_stream_mux_handle_sink_event(GstPad* pad, GstObject* parent, GstEvent* event);
static gboolean gst_nv_auto_stream_mux_handle_src_event(GstPad* pad, GstObject* parent, GstEvent* event);
static gboolean gst_nv_auto_stream_mux_set_caps(GstNvAutoStreamMux* filter, const GstCaps* caps);
static GstFlowReturn gst_nv_auto_stream_mux_video_chain(GstPad* pad, GstObject* parent, GstBuffer* buf);

static NvDsBatchMeta* add_batch_meta_to_buffer(GstBuffer* buffer);

static void
gst_nv_auto_stream_mux_class_init(GstNvAutoStreamMuxClass* klass)
{
    GObjectClass* gobject_class = G_OBJECT_CLASS(klass);
    GstElementClass* element_class = GST_ELEMENT_CLASS(klass);


    gst_element_class_add_static_pad_template(element_class, &src_factory);
    gst_element_class_add_static_pad_template(element_class, &sink_factory);

    gst_element_class_set_static_metadata(
        element_class,
        "nvautostreammux",
        "Auto Stream Mux",
        "Mux Batch Metadata into the stream without specifying dimensions",
        "Andrew Knuthup"
    );

    gobject_class->set_property = gst_nv_auto_stream_mux_set_property;
    gobject_class->get_property = gst_nv_auto_stream_mux_get_property;
    gobject_class->finalize = gst_nv_auto_stream_mux_finalize;
}

static void
gst_nv_auto_stream_mux_init(GstNvAutoStreamMux* filter)
{
    filter->sinkpad = gst_pad_new_from_static_template(&sink_factory, "sink");
    gst_pad_set_chain_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_nv_auto_stream_mux_video_chain));
    gst_pad_set_event_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_nv_auto_stream_mux_handle_sink_event));
    gst_element_add_pad(GST_ELEMENT(filter), filter->sinkpad);

    filter->srcpad = gst_pad_new_from_static_template(&src_factory, "src");
    gst_pad_set_event_function(filter->srcpad, GST_DEBUG_FUNCPTR(gst_nv_auto_stream_mux_handle_src_event));
    gst_element_add_pad(GST_ELEMENT(filter), filter->srcpad);
}

static void
gst_nv_auto_stream_mux_finalize(GObject* object)
{
}

static void
gst_nv_auto_stream_mux_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
    GstNvAutoStreamMux* filter = GST_NV_AUTO_STREAM_MUX_CAST(object);

    switch (prop_id)
    {
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void
gst_nv_auto_stream_mux_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
    GstNvAutoStreamMux* filter = GST_NV_AUTO_STREAM_MUX_CAST(object);

    switch (prop_id)
    {
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static gboolean
gst_nv_auto_stream_mux_handle_src_event(GstPad* pad, GstObject* parent, GstEvent* event)
{
    GstNvAutoStreamMux* filter = GST_NV_AUTO_STREAM_MUX_CAST(parent);
    return gst_pad_push_event(filter->sinkpad, event);
}

static gboolean
gst_nv_auto_stream_mux_handle_sink_event(GstPad* pad, GstObject* parent, GstEvent* event)
{
    GstNvAutoStreamMux* filter = GST_NV_AUTO_STREAM_MUX_CAST(parent);
    return gst_pad_push_event(filter->srcpad, event);
}

static GstFlowReturn
gst_nv_auto_stream_mux_video_chain(GstPad* pad, GstObject* parent, GstBuffer* buffer)
{
    GstNvAutoStreamMux* filter = GST_NV_AUTO_STREAM_MUX_CAST(parent);

    // Allow passthrough is the batch metadata is already muxed in
    NvDsBatchMeta* batch_meta = gst_buffer_get_nvds_batch_meta(buffer);
    if (!batch_meta)
    {
        batch_meta = add_batch_meta_to_buffer(buffer);
        if (!batch_meta)
        {
            gchar* name = gst_object_get_name(GST_OBJECT_CAST(filter));
            GST_ERROR(
                "%s: Could not mux the batch metadata into the stream.  Try "
                "using nvstreammux instead.", name ? name : "(null)"
            );
            g_free(name);
            return GST_FLOW_ERROR;
        }
    }

    return gst_pad_push(filter->srcpad, buffer);
}

static NvDsBatchMeta* add_batch_meta_to_buffer(GstBuffer* buffer)
{
    const guint frames_per_batch = 1;
    NvDsBatchMeta* batch_meta = nvds_create_batch_meta(frames_per_batch);
    if (!batch_meta)
    {
        return nullptr;
    }

    batch_meta->max_frames_in_batch = frames_per_batch;

    NvDsFrameMeta* frame_meta = nvds_acquire_frame_meta_from_pool(batch_meta);
    nvds_add_frame_meta_to_batch(batch_meta, frame_meta);

    NvDsMeta* dsmeta = gst_buffer_add_nvds_meta(
                           buffer, batch_meta, NULL,
                           nvds_batch_meta_copy_func,
                           nvds_batch_meta_release_func
                       );
    dsmeta->meta_type = NVDS_BATCH_GST_META;

    // TODO: Verify that not setting the meta_tranform_func or
    //       meta_release_func is well-defined for NVDS_BATCH_GST_META since it
    //       is a built-in type.  This seems to work fine but would not work
    //       for a User or Custom meta_type.
    return batch_meta;
}
