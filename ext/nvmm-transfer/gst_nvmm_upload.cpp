/*
# Copyright (c) 2020, Andrew Knuthup
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SECTION:element-nvmmupload
 *
 * Upload frames from the host to the device
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v videotestsrc ! nvmmupload ! nvegltransform ! nveglglessink
 * ]|
 * </refsect2>
 */

#include "gst_nvmm_upload.h"

#include <cstdint>
#include <cstring>

#include <gst/gst.h>
#include <gst/video/video.h>

#include <nvbuf_utils.h>
#include <cuda_runtime_api.h>
#include <nvmm_interop/egl.hpp>
#include <nvmm_interop/utility.h>


#define GST_CAT_DEFAULT gst_nvmm_upload_debug

enum
{
    PROP_0
};

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE(
    "sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(GST_VIDEO_CAPS_MAKE("RGBA"))  // TODO: Support more formats
);

#define GST_CAPS_FEATURE_MEMORY_NVMM "memory:NVMM"
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE(
    "src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(
        GST_VIDEO_CAPS_MAKE_WITH_FEATURES(
            GST_CAPS_FEATURE_MEMORY_NVMM,
            "{ RGBA }"
        )
    )
);

#define _do_init \
    GST_DEBUG_CATEGORY_INIT(gst_nvmm_upload_debug, "nvmmupload", 0, "Nvmm Upload");
#define gst_nvmm_upload_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE(GstNvmmUpload, gst_nvmm_upload, GST_TYPE_ELEMENT, _do_init)

static void gst_nvmm_upload_finalize(GObject* object);
static void gst_nvmm_upload_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec);
static void gst_nvmm_upload_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec);
static gboolean gst_nvmm_upload_handle_sink_event(GstPad* pad, GstObject* parent, GstEvent* event);
static gboolean gst_nvmm_upload_handle_src_event(GstPad* pad, GstObject* parent, GstEvent* event);
static GstFlowReturn gst_nvmm_upload_video_chain(GstPad* pad, GstObject* parent, GstBuffer* buf);

static gboolean get_frame_dimensions(GstPad& pad_ref, gint& width, gint& height);
static GstBuffer* upload(GstBuffer& host_buffer_ref, const gint width, const gint height);
static void destroy_nv_buffer(gpointer user_data);
static GstBuffer* create_nvmm_buffer(const std::int32_t width, const std::int32_t height, const std::int32_t num_channels);
static GstBuffer* wrap_nvmm_buffer(const int dmabuf_fd);


static void
gst_nvmm_upload_class_init(GstNvmmUploadClass* klass)
{
    GObjectClass* gobject_class = G_OBJECT_CLASS(klass);
    GstElementClass* element_class = GST_ELEMENT_CLASS(klass);


    gst_element_class_add_static_pad_template(element_class, &src_factory);
    gst_element_class_add_static_pad_template(element_class, &sink_factory);

    gst_element_class_set_static_metadata(
        element_class,
        "nvmmupload",
        "NVMM upload",
        "Upload host frames to NVMM without any fancy conversions",
        "Andrew Knuthup"
    );

    gobject_class->set_property = gst_nvmm_upload_set_property;
    gobject_class->get_property = gst_nvmm_upload_get_property;
    gobject_class->finalize = gst_nvmm_upload_finalize;
}


static void
gst_nvmm_upload_init(GstNvmmUpload* filter)
{
    filter->sinkpad = gst_pad_new_from_static_template(&sink_factory, "sink");
    gst_pad_set_chain_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_nvmm_upload_video_chain));
    gst_pad_set_event_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_nvmm_upload_handle_sink_event));
    gst_element_add_pad(GST_ELEMENT(filter), filter->sinkpad);

    filter->srcpad = gst_pad_new_from_static_template(&src_factory, "src");
    gst_pad_set_event_function(filter->srcpad, GST_DEBUG_FUNCPTR(gst_nvmm_upload_handle_src_event));
    gst_element_add_pad(GST_ELEMENT(filter), filter->srcpad);
}


static void
gst_nvmm_upload_finalize(GObject* object)
{
}


static void
gst_nvmm_upload_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
    GstNvmmUpload* filter = GST_NVMM_UPLOAD_CAST(object);

    switch (prop_id)
    {
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}


static void
gst_nvmm_upload_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
    GstNvmmUpload* filter = GST_NVMM_UPLOAD_CAST(object);

    switch (prop_id)
    {
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}


static gboolean
gst_nvmm_upload_handle_src_event(GstPad* pad, GstObject* parent, GstEvent* event)
{
    GstNvmmUpload* filter = GST_NVMM_UPLOAD_CAST(parent);
    return gst_pad_push_event(filter->sinkpad, event);
}


static gboolean
gst_nvmm_upload_handle_sink_event(GstPad* pad, GstObject* parent, GstEvent* event)
{
    GstNvmmUpload* filter = GST_NVMM_UPLOAD_CAST(parent);
    switch (GST_EVENT_TYPE(event))
    {
        case GST_EVENT_CAPS:
        {
            // Add NVMM caps features
            GstCaps* sink_caps = nullptr;
            gst_event_parse_caps(event, &sink_caps);
            GstCaps* src_caps = gst_caps_copy(sink_caps);

            gst_event_unref(event);

            GstCapsFeatures* features =
                gst_caps_features_from_string(GST_CAPS_FEATURE_MEMORY_NVMM);
            gst_caps_set_features(src_caps, 0, features);
            GstEvent* src_event = gst_event_new_caps(src_caps);

            return gst_pad_push_event(filter->srcpad, src_event);
        }

        default:
            return gst_pad_push_event(filter->srcpad, event);
    }

    return FALSE;
}


static GstFlowReturn
gst_nvmm_upload_video_chain(GstPad* pad, GstObject* parent, GstBuffer* buffer)
{
    GstNvmmUpload* filter = GST_NVMM_UPLOAD_CAST(parent);

    gint width = -1;
    gint height = -1;
    g_return_val_if_fail(
        get_frame_dimensions(*pad, width, height), GST_FLOW_ERROR
    );

    auto device_buffer = upload(*buffer, width, height);
    return gst_pad_push(filter->srcpad, device_buffer);
}


static gboolean
get_frame_dimensions(GstPad& pad_ref, gint& width, gint& height)
{
    GstPad* pad = &pad_ref;

    GstCaps* caps = gst_pad_get_current_caps(pad);
    GstStructure* structure = gst_caps_get_structure(caps, 0);

    gboolean rc = gst_structure_get_int(structure, "width", &width);
    rc &= gst_structure_get_int(structure, "height", &height);
    gst_caps_unref(caps);

    return rc;
}


static GstBuffer*
upload(GstBuffer& host_buffer_ref, const gint width, const gint height)
{
    GstBuffer* host_buffer = &host_buffer_ref;

    static constexpr const std::size_t num_host_channels { 4 };
    auto device_buffer =
        create_nvmm_buffer(
            width,
            height,
            num_host_channels
        );

    // TODO: Use scoped mapper
    GstMapInfo info;
    gst_buffer_map(host_buffer, &info, GST_MAP_READ);

    nvmm::egl::Handle device_handle;
    nvmm::egl::map<GST_MAP_WRITE>(device_handle, *device_buffer);
    auto& device_frame = device_handle.frame;

    CUDA_SAFE_CALL(
        cudaMemcpy2D(
            device_frame.frame.pPitch[0],
            device_frame.pitch,
            info.data,
            GST_ROUND_UP_4(num_host_channels * width),
            num_host_channels * width,
            height,
            cudaMemcpyHostToDevice
        )
    );

    nvmm::egl::unmap(device_handle, *device_buffer);
    gst_buffer_unmap(host_buffer, &info);

    gst_buffer_copy_into(
        device_buffer, host_buffer,
        static_cast<GstBufferCopyFlags>(GST_BUFFER_COPY_METADATA),
        0, -1
    );

    gst_buffer_unref(host_buffer);
    return device_buffer;
}


static void
destroy_nv_buffer(gpointer user_data)
{
    int dmabuf_fd = 0;
    std::memcpy(&dmabuf_fd, user_data, sizeof dmabuf_fd);
    JETSON_MM_SAFE_CALL(NvBufferDestroy(dmabuf_fd));
    g_free(user_data);
}


static GstBuffer*
create_nvmm_buffer(const std::int32_t width, const std::int32_t height, const std::int32_t num_channels)
{
    NvBufferCreateParams params;
    params.width = width;
    params.height = height;
    params.payloadType = NvBufferPayload_SurfArray;
    params.memsize =
        params.width * params.height * num_channels * sizeof(std::uint8_t);
    params.layout = NvBufferLayout_Pitch;
    params.colorFormat = NvBufferColorFormat_ABGR32;
    params.nvbuf_tag = NvBufferTag_NONE;

    int dmabuf_fd = 0;
    JETSON_MM_SAFE_CALL(NvBufferCreateEx(&dmabuf_fd, &params));

    return wrap_nvmm_buffer(dmabuf_fd);
}


static GstBuffer*
wrap_nvmm_buffer(const int dmabuf_fd)
{
    auto user_data = g_new(int, 1);
    *user_data = dmabuf_fd;

    NvBufferParams params;
    JETSON_MM_SAFE_CALL(NvBufferGetParams(dmabuf_fd, &params));
    gpointer data = g_malloc(params.nv_buffer_size);
    if (data == nullptr)
    {
        return nullptr;
    }

    GstMemoryFlags flags = static_cast<GstMemoryFlags>(0);
    GstBuffer* buffer =
        gst_buffer_new_wrapped_full(
            flags,
            data,
            params.nv_buffer_size,
            0,
            params.nv_buffer_size,
            user_data,
            destroy_nv_buffer
        );

    if (buffer == nullptr)
    {
        return nullptr;
    }

    GstMapInfo info;
    gst_buffer_map(buffer, &info, GST_MAP_WRITE);
    std::memcpy(info.data, params.nv_buffer, params.nv_buffer_size);
    gst_buffer_unmap(buffer, &info);

    return buffer;
}
