/*
# Copyright (c) 2020, Andrew Knuthup
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <gst/gst.h>

G_BEGIN_DECLS

#define GST_TYPE_NVMM_DOWNLOAD \
    (gst_nvmm_download_get_type())
#define GST_NVMM_DOWNLOAD_CAST(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_NVMM_DOWNLOAD,GstNvmmDownload))
#define GST_NVMM_DOWNLOAD_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_NVMM_DOWNLOAD,GstNvmmDownloadClass))
#define GST_IS_NVMM_DOWNLOAD(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_NVMM_DOWNLOAD))
#define GST_IS_NVMM_DOWNLOAD_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_NVMM_DOWNLOAD))

GST_DEBUG_CATEGORY_EXTERN(gst_nvmm_download_debug);

typedef struct _GstNvmmDownload GstNvmmDownload;
typedef struct _GstNvmmDownloadClass GstNvmmDownloadClass;

struct _GstNvmmDownload
{
    GstElement element;

    GstPad* sinkpad;
    GstPad* srcpad;
};

struct _GstNvmmDownloadClass
{
    GstElementClass parent_class;
};

GType gst_nvmm_download_get_type(void);

G_END_DECLS
