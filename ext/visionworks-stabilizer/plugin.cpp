#include "gst_visionworks_stabilizer.h"

GST_DEBUG_CATEGORY(gst_visionworks_stabilizer_debug);

static gboolean
plugin_init(GstPlugin* plugin)
{
    return
        gst_element_register(
            plugin,
            "nvvwstabilizer",
            GST_RANK_NONE,
            GST_TYPE_VISIONWORKS_STABILIZER
        );
}

GST_PLUGIN_DEFINE(
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    PLUGIN_NAME,
    "Stabilize Video",
    plugin_init,
    VERSION,
    "BSD",
    GST_PACKAGE_NAME,
    GST_PACKAGE_ORIGIN
);
