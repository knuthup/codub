/*
# Copyright (c) 2020, Andrew Knuthup
# Copyright (c) 2014-2016, NVIDIA CORPORATION
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * SECTION:element-nvvwstabilizer
 *
 * Stabilize Video
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch-1.0 -v videotestsrc ! nvvidconv ! nvvwstabilizer ! nvvidconv ! ximagesink
 * ]|
 * </refsect2>
 */

#include "gst_visionworks_stabilizer.h"
#include "stabilizer.hpp"

#include <gst/gst.h>
#include <gst/video/video.h>

#include <OVX/UtilityOVX.hpp>
#include <nvmm_interop/vx.hpp>


#define GST_CAT_DEFAULT gst_visionworks_stabilizer_debug

static constexpr const guint DEFAULT_NUM_SMOOTHING_FRAMES { 5 };
static constexpr const guint FRAME_DELAY_OFFSET { 2 };
static constexpr const guint DEFAULT_NUM_DELAYED_FRAMES { DEFAULT_NUM_SMOOTHING_FRAMES + FRAME_DELAY_OFFSET };
static constexpr const gfloat DEFAULT_MAX_FRAME_CROP_MARGIN { 0.07f };
static constexpr const gboolean DEFAULT_ADJUST_TIMESTAMPS { TRUE };

enum
{
    PROP_0,
    PROP_NUM_SMOOTHING_FRAMES,
    PROP_NUM_DELAYED_FRAMES,
    PROP_MAX_FRAME_CROP_MARGIN,
    PROP_ADJUST_TIMESTAMPS
};

#define GST_CAPS_FEATURE_MEMORY_NVMM "memory:NVMM"
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE(
    "sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(
        GST_VIDEO_CAPS_MAKE_WITH_FEATURES(
            GST_CAPS_FEATURE_MEMORY_NVMM,
            "{ RGBA }"
        )
    )
);

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE(
    "src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS(
        GST_VIDEO_CAPS_MAKE_WITH_FEATURES(
            GST_CAPS_FEATURE_MEMORY_NVMM,
            "{ RGBA }"
        )
    )
);

#define _do_init \
    GST_DEBUG_CATEGORY_INIT(gst_visionworks_stabilizer_debug, "nvvwstabilizer", 0, "VisionWorks Stabilizer");
#define gst_visionworks_stabilizer_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE(GstVisionWorksStabilizer, gst_visionworks_stabilizer, GST_TYPE_ELEMENT, _do_init)

static void gst_visionworks_stabilizer_finalize(GObject* object);
static void gst_visionworks_stabilizer_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec);
static void gst_visionworks_stabilizer_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec);
static gboolean gst_visionworks_stabilizer_handle_sink_event(GstPad* pad, GstObject* parent, GstEvent* event);
static gboolean gst_visionworks_stabilizer_handle_src_event(GstPad* pad, GstObject* parent, GstEvent* event);
static GstFlowReturn gst_visionworks_stabilizer_video_chain(GstPad* pad, GstObject* parent, GstBuffer* buf);

static void initialize_stabilizer(GstVisionWorksStabilizer& stabilizer, GstBuffer& buffer_ref);


static void
gst_visionworks_stabilizer_class_init(GstVisionWorksStabilizerClass* klass)
{
    GObjectClass* gobject_class = G_OBJECT_CLASS(klass);
    GstElementClass* element_class = GST_ELEMENT_CLASS(klass);

    gst_element_class_add_static_pad_template(element_class, &src_factory);
    gst_element_class_add_static_pad_template(element_class, &sink_factory);

    gst_element_class_set_static_metadata(
        element_class,
        "nvvwstabilizer",
        "Stabilize Video",
        "Stabilize Video",
        "Andrew Knuthup"
    );

    gobject_class->set_property = GST_DEBUG_FUNCPTR(gst_visionworks_stabilizer_set_property);
    gobject_class->get_property = GST_DEBUG_FUNCPTR(gst_visionworks_stabilizer_get_property);
    gobject_class->finalize = GST_DEBUG_FUNCPTR(gst_visionworks_stabilizer_finalize);

    g_object_class_install_property(
        gobject_class,
        PROP_NUM_SMOOTHING_FRAMES,
        g_param_spec_uint(
            "num-smoothing-frames",
            "Num Smoothing Frames",
            "The number of smoothing frames for stabilization",
            1,
            128,
            DEFAULT_NUM_SMOOTHING_FRAMES,
            G_PARAM_READWRITE
        )
    );

    g_object_class_install_property(
        gobject_class,
        PROP_NUM_DELAYED_FRAMES,
        g_param_spec_uint(
            "num-delayed-frames",
            "Num Delayed Frames",
            "The output of the stabilizer is delayed by this many frames",
            3,
            130,
            DEFAULT_NUM_DELAYED_FRAMES,
            G_PARAM_READABLE
        )
    );

    g_object_class_install_property(
        gobject_class,
        PROP_MAX_FRAME_CROP_MARGIN,
        g_param_spec_float(
            "max-crop-margin",
            "Maximum frame crop margin",
            "The maximum ratio of pixels in a frame to be cropped for stabilization",
            0.0,
            1.0,
            DEFAULT_MAX_FRAME_CROP_MARGIN,
            G_PARAM_READWRITE
        )
    );

    g_object_class_install_property(
        gobject_class,
        PROP_ADJUST_TIMESTAMPS,
        g_param_spec_boolean(
            "adjust-timestamps",
            "Adjust timestamps",
            "Adjust timestamps to offset the delay produced by the stabilizer",
            DEFAULT_ADJUST_TIMESTAMPS,
            G_PARAM_READWRITE
        )
    );
}


static void
gst_visionworks_stabilizer_init(GstVisionWorksStabilizer* filter)
{
    filter->sinkpad = gst_pad_new_from_static_template(&sink_factory, "sink");
    gst_pad_set_chain_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_visionworks_stabilizer_video_chain));
    gst_pad_set_event_function(filter->sinkpad, GST_DEBUG_FUNCPTR(gst_visionworks_stabilizer_handle_sink_event));
    gst_element_add_pad(GST_ELEMENT(filter), filter->sinkpad);

    filter->srcpad = gst_pad_new_from_static_template(&src_factory, "src");
    gst_pad_set_event_function(filter->srcpad, GST_DEBUG_FUNCPTR(gst_visionworks_stabilizer_handle_src_event));
    gst_element_add_pad(GST_ELEMENT(filter), filter->srcpad);

    filter->context = std::make_unique<ovxio::ContextGuard>();
    auto& context = *filter->context;
    vxRegisterLogCallback(context, &ovxio::stdoutLogCallback, vx_false_e);
    vxDirective(context, VX_DIRECTIVE_ENABLE_PERFORMANCE);

    std::exchange(filter->delayed_timestamps, decltype(filter->delayed_timestamps)());
    std::exchange(filter->stabilizer, decltype(filter->stabilizer)());

    filter->initialized = FALSE;

    // properties
    filter->num_smoothing_frames = DEFAULT_NUM_SMOOTHING_FRAMES;
    filter->num_delayed_frames = DEFAULT_NUM_DELAYED_FRAMES;
    filter->max_frame_crop_margin = DEFAULT_MAX_FRAME_CROP_MARGIN;
    filter->adjust_timestamps = DEFAULT_ADJUST_TIMESTAMPS;
}


static void
gst_visionworks_stabilizer_finalize(GObject* object)
{
    GstVisionWorksStabilizer* filter = GST_VISIONWORKS_STABILIZER_CAST(object);

    filter->stabilizer.reset(nullptr);
    filter->context.reset(nullptr);
}


static void
gst_visionworks_stabilizer_set_property(GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
    GstVisionWorksStabilizer* filter = GST_VISIONWORKS_STABILIZER_CAST(object);

    switch (prop_id)
    {
        case PROP_NUM_SMOOTHING_FRAMES:
            filter->num_smoothing_frames = g_value_get_uint(value);
            filter->num_delayed_frames = filter->num_smoothing_frames + FRAME_DELAY_OFFSET;
            break;

        case PROP_MAX_FRAME_CROP_MARGIN:
            filter->max_frame_crop_margin = g_value_get_float(value);
            break;

        case PROP_ADJUST_TIMESTAMPS:
            filter->adjust_timestamps = g_value_get_boolean(value);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}


static void
gst_visionworks_stabilizer_get_property(GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
    GstVisionWorksStabilizer* filter = GST_VISIONWORKS_STABILIZER_CAST(object);

    switch (prop_id)
    {
        case PROP_NUM_SMOOTHING_FRAMES:
            g_value_set_uint(value, filter->num_smoothing_frames);
            break;

        case PROP_NUM_DELAYED_FRAMES:
            g_value_set_uint(value, filter->num_delayed_frames);
            break;

        case PROP_MAX_FRAME_CROP_MARGIN:
            g_value_set_float(value, filter->max_frame_crop_margin);
            break;

        case PROP_ADJUST_TIMESTAMPS:
            g_value_set_boolean(value, filter->adjust_timestamps);
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}


static gboolean
gst_visionworks_stabilizer_handle_src_event(GstPad* pad, GstObject* parent, GstEvent* event)
{
    GstVisionWorksStabilizer* filter = GST_VISIONWORKS_STABILIZER_CAST(parent);
    switch (GST_EVENT_TYPE(event))
    {
        default:
            return gst_pad_push_event(filter->sinkpad, event);
    }

    return FALSE;
}


static gboolean
gst_visionworks_stabilizer_handle_sink_event(GstPad* pad, GstObject* parent, GstEvent* event)
{
    GstVisionWorksStabilizer* filter = GST_VISIONWORKS_STABILIZER_CAST(parent);
    switch (GST_EVENT_TYPE(event))
    {
        default:
            return gst_pad_push_event(filter->srcpad, event);
    }

    return FALSE;
}


static GstFlowReturn
gst_visionworks_stabilizer_video_chain(GstPad* pad, GstObject* parent, GstBuffer* buffer)
{
    GstVisionWorksStabilizer* filter = GST_VISIONWORKS_STABILIZER_CAST(parent);

    auto& context = *filter->context;

    if (filter->initialized == FALSE)
    {
        initialize_stabilizer(*filter, *buffer);
    }

    buffer = gst_buffer_make_writable(buffer);

    // Read and process the frame
    nvmm::vx::Handle handle;
    nvmm::vx::map(handle, context, *buffer);

    filter->stabilizer->process(handle.image);

    filter->delayed_timestamps.push_back(GST_BUFFER_PTS(buffer));
    if (filter->delayed_timestamps.size() < filter->num_delayed_frames)
    {
        if (filter->adjust_timestamps)
        {
            // Alternative methods to wait for the stabilizer latency
#if 1
            nvmm::vx::unmap(handle, *buffer);

            // Do not push out a buffer until we receive enough buffers to
            // perform synchronized stabilization.
            gst_buffer_unref(buffer);
            return GST_FLOW_OK;
#else
            // Keep pushing out the current buffer with the first timestamp
            // received until we receive enough buffers to perform synchronized
            // stabilization.
            auto first_ts = filter->delayed_timestamps.front();
            filter->delayed_timestamps.push_front(first_ts);
#endif
        }
    }

    GST_BUFFER_PTS(buffer) = filter->delayed_timestamps.front();
    filter->delayed_timestamps.pop_front();

    vx_image stabilized_frame = filter->stabilizer->getStabilizedFrame();
    // TODO: Eliminate the copy below.  Options:
    //
    //       1) NvBuffer API.  Unlikely as no method seems to support mapping
    //          device memory to new buffers.  Memory allocation is the only
    //          entrypoint for creating new buffers.
    //
    //       2) Modify the stabilizer to eliminate the internal perspective
    //          transform and instead provide the reference image and the
    //          perspective matrix in order to output directly to a non-graph
    //          destination.

    // Copy the result into the buffer
    nvxuCopyImage(context, stabilized_frame, handle.image);
    nvmm::vx::unmap(handle, *buffer);

    return gst_pad_push(filter->srcpad, buffer);
}


static void
initialize_stabilizer(GstVisionWorksStabilizer& stabilizer, GstBuffer& buffer_ref)
{
    GstVisionWorksStabilizer* filter = &stabilizer;
    GstBuffer* buffer = &buffer_ref;

    auto& context = *filter->context;

    // Read the first frame
    nvmm::vx::Handle handle;
    nvmm::vx::map<GST_MAP_READ>(handle, context, *buffer);

    // Create VideoStabilizer instance
    nvx::VideoStabilizer::VideoStabilizerParams params;
    params.numOfSmoothingFrames_ = filter->num_smoothing_frames;
    params.cropMargin_ = filter->max_frame_crop_margin;

    filter->stabilizer = std::unique_ptr<nvx::VideoStabilizer>(nvx::VideoStabilizer::createImageBasedVStab(context, params));
    filter->stabilizer->init(handle.image);
    nvmm::vx::unmap(handle, *buffer);

    filter->initialized = true;
}
