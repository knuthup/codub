# codub

A collection of GStreamer plugins to perform hardware-accelerated tasks such as video stabilization, feature tracking, object tracking, and motion estimation on the NVIDIA Jetson Platform.
