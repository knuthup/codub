find_package(PkgConfig REQUIRED)
pkg_check_modules(PC_GST REQUIRED gstreamer-base-1.0>=1.10)

set(GStreamer_INCLUDE_DIR ${PC_GST_INCLUDE_DIRS})
set(GStreamer_LIBRARY ${PC_GST_LIBRARIES})
set(GStreamer_LIBRARY_DIR ${PC_GST_LIBRARY_DIRS})

mark_as_advanced(
    GStreamer_INCLUDE_DIR
    GStreamer_LIBRARY
    GStreamer_LIBRARY_DIR
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GStreamer
    REQUIRED_VARS
        GStreamer_INCLUDE_DIR
        GStreamer_LIBRARY
)

set(GStreamer_INCLUDE_DIRS ${GStreamer_INCLUDE_DIR})
set(GStreamer_LIBRARIES ${GStreamer_LIBRARY})
set(GStreamer_LIBRARY_DIRS ${GStreamer_LIBRARY_DIR})

pkg_get_variable(GStreamer_PLUGINS_DIR gstreamer-1.0 pluginsdir)

if(GStreamer_FOUND AND NOT TARGET GStreamer::gstreamer)
    add_library(GStreamer::gstreamer INTERFACE IMPORTED)
    set_target_properties(GStreamer::gstreamer PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${GStreamer_INCLUDE_DIRS}"
        INTERFACE_LINK_LIBRARIES "${GStreamer_LIBRARIES}"
        INTERFACE_LINK_DIRECTORIES "${GStreamer_LIBRARY_DIRS}"
    )

    if(NOT TARGET GStreamer::base)
        add_library(GStreamer::base INTERFACE IMPORTED)
        set_target_properties(GStreamer::base PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${GStreamer_INCLUDE_DIRS}"
            INTERFACE_LINK_LIBRARIES "${GStreamer_LIBRARIES}"
            INTERFACE_LINK_DIRECTORIES "${GStreamer_LIBRARY_DIRS}"
        )
    endif()
endif()

list(REMOVE_DUPLICATES GStreamer_FIND_COMPONENTS)
list(TRANSFORM GStreamer_FIND_COMPONENTS TOLOWER)
list(REMOVE_ITEM GStreamer_FIND_COMPONENTS base gstreamer)

foreach(LOWERCOMPONENT ${GStreamer_FIND_COMPONENTS})
    string(TOUPPER ${LOWERCOMPONENT} UPPERCOMPONENT)

    pkg_check_modules(PC_GST_${UPPERCOMPONENT} REQUIRED gstreamer-${LOWERCOMPONENT}-1.0>=1.10)

    set(GStreamer_${UPPERCOMPONENT}_INCLUDE_DIR ${PC_GST_${UPPERCOMPONENT}_INCLUDE_DIRS})
    set(GStreamer_${UPPERCOMPONENT}_LIBRARY ${PC_GST_${UPPERCOMPONENT}_LIBRARIES})
    set(GStreamer_${UPPERCOMPONENT}_LIBRARY_DIR ${PC_GST_${UPPERCOMPONENT}_LIBRARY_DIRS})

    mark_as_advanced(
        GStreamer_${UPPERCOMPONENT}_INCLUDE_DIR
        GStreamer_${UPPERCOMPONENT}_LIBRARY
        GStreamer_${UPPERCOMPONENT}_LIBRARY_DIR
    )

    find_package_handle_standard_args(GStreamer_${UPPERCOMPONENT}
        REQUIRED_VARS
            GStreamer_${UPPERCOMPONENT}_INCLUDE_DIR
            GStreamer_${UPPERCOMPONENT}_LIBRARY
        NAME_MISMATCHED
    )

    set(GStreamer_${UPPERCOMPONENT}_INCLUDE_DIRS ${GStreamer_${UPPERCOMPONENT}_INCLUDE_DIR})
    set(GStreamer_${UPPERCOMPONENT}_LIBRARIES ${GStreamer_${UPPERCOMPONENT}_LIBRARY})
    set(GStreamer_${UPPERCOMPONENT}_LIBRARY_DIRS ${GStreamer_${UPPERCOMPONENT}_LIBRARY_DIR})

    if(GStreamer_${UPPERCOMPONENT}_FOUND AND NOT TARGET GStreamer::${LOWERCOMPONENT})
        add_library(GStreamer::${LOWERCOMPONENT} INTERFACE IMPORTED)
        set_target_properties(GStreamer::${LOWERCOMPONENT} PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${GStreamer_${UPPERCOMPONENT}_INCLUDE_DIRS}"
            INTERFACE_LINK_LIBRARIES "${GStreamer_${UPPERCOMPONENT}_LIBRARIES}"
            INTERFACE_LINK_DIRECTORIES "${GStreamer_${UPPERCOMPONENT}_LIBRARY_DIRS}"
        )
    endif()
endforeach()

foreach(LOWERCOMPONENT ${GStreamer_FIND_COMPONENTS})
    list(APPEND GStreamer_INCLUDE_DIRS ${GStreamer_${UPPERCOMPONENT}_INCLUDE_DIRS})
    list(APPEND GStreamer_LIBRARIES ${GStreamer_${UPPERCOMPONENT}_LIBRARIES})
    list(APPEND GStreamer_LIBRARY_DIRS ${GStreamer_${UPPERCOMPONENT}_LIBRARY_DIRS})
endforeach()

list(REMOVE_DUPLICATES GStreamer_INCLUDE_DIRS)
list(REMOVE_DUPLICATES GStreamer_LIBRARIES)
list(REMOVE_DUPLICATES GStreamer_LIBRARY_DIRS)
