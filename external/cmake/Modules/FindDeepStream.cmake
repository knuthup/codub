set(DeepStream_ROOT "/opt/nvidia/deepstream/deepstream-4.0" CACHE FILEPATH "Path to deepstream")
set(DeepStream_NvDsGstMeta_LIBRARY_NAME nvdsgst_meta)
set(DeepStream_NvDsMeta_LIBRARY_NAME nvds_meta)

find_path(DeepStream_INCLUDE_DIR nvdsmeta.h
    HINTS ${DeepStream_ROOT}/sources/includes
)

find_library(DeepStream_NVDSGSTMETA_LIB
    NAMES ${DeepStream_NvDsGstMeta_LIBRARY_NAME}
    HINTS ${DeepStream_ROOT}/lib/
)

find_library(DeepStream_NVDSMETA_LIB
    NAMES ${DeepStream_NvDsMeta_LIBRARY_NAME}
    HINTS ${DeepStream_ROOT}/lib/
)

mark_as_advanced(
    DeepStream_ROOT
    DeepStream_NVDSMETA_LIB
    DeepStream_NvDsMeta_LIBRARY_NAME
    DeepStream_NVDSGSTMETA_LIB
    DeepStream_NvDsGstMeta_LIBRARY_NAME
    DeepStream_FOUND
    DeepStream_INCLUDE_DIR
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(DeepStream
    REQUIRED_VARS
        DeepStream_INCLUDE_DIR
        DeepStream_NVDSGSTMETA_LIB
        DeepStream_NVDSMETA_LIB
)

if(DeepStream_FOUND)
    set(DeepStream_INCLUDE_DIRS ${DeepStream_INCLUDE_DIR})
    set(DeepStream_LIBRARIES
        ${DeepStream_NVDSGSTMETA_LIB}
        ${DeepStream_NVDSMETA_LIB}
    )
endif()

if(DeepStream_FOUND AND NOT TARGET DeepStream::DeepStream)
    add_library(DeepStream::DeepStream INTERFACE IMPORTED)
    set_target_properties(DeepStream::DeepStream PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${DeepStream_INCLUDE_DIR}"
        INTERFACE_LINK_LIBRARIES "${DeepStream_LIBRARIES}"
    )
endif()
