find_package(CUDAToolkit REQUIRED)

find_path(VisionWorks_INCLUDE_DIR NAME NVX/nvx.h HINTS NONE)
find_library(VisionWorks_LIBRARY NAME visionworks HINTS NONE)

mark_as_advanced(
    VisionWorks_INCLUDE_DIR
    VisionWorks_LIBRARY
    VisionWorks_FOUND
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(VisionWorks
    REQUIRED_VARS
        VisionWorks_INCLUDE_DIR
        VisionWorks_LIBRARY
)

if(VisionWorks_FOUND)
    get_target_property(CUDART_INCLUDE_DIRS CUDA::cudart INTERFACE_INCLUDE_DIRECTORIES)
    set(VisionWorks_INCLUDE_DIRS ${VisionWorks_INCLUDE_DIR} ${CUDART_INCLUDE_DIRS})
    set(VisionWorks_LIBRARIES ${VisionWorks_LIBRARY})
endif()

if(VisionWorks_FOUND AND NOT TARGET VisionWorks::visionworks)
    add_library(VisionWorks::visionworks INTERFACE IMPORTED)
    set_target_properties(VisionWorks::visionworks PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${VisionWorks_INCLUDE_DIRS}"
        INTERFACE_LINK_LIBRARIES "${VisionWorks_LIBRARIES}"
    )
endif()
