# Copyright (c) 2014-2016, NVIDIA CORPORATION. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#  * Neither the name of NVIDIA CORPORATION nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


add_subdirectory(app)

find_package(VisionWorks REQUIRED)

set(FEATURETRACKERNVXCU_LIBRARY_NAME "visionworks_featuretrackernvxcu")

# Copy necessary headers to be accessed by other projects
set(FEATURETRACKERNVXCU_EXPORTED_HEADERS feature_tracker_nvxcu.hpp)

foreach(SRC_HEADER ${FEATURETRACKERNVXCU_EXPORTED_HEADERS})
    set(DST_HEADER ${DEMOS_NVX_PUBLIC_INCLUDE_DIRS}/${SRC_HEADER})
    configure_file(${SRC_HEADER} ${DST_HEADER} COPYONLY)
endforeach()

set(FEATURETRACKERNVXCU_srcs
    feature_tracker_nvxcu.cpp
)

add_library(${FEATURETRACKERNVXCU_LIBRARY_NAME} STATIC ${FEATURETRACKERNVXCU_srcs})
add_library(VisionWorks::feature_tracker_nvxcu ALIAS ${FEATURETRACKERNVXCU_LIBRARY_NAME})

set_target_properties(${FEATURETRACKERNVXCU_LIBRARY_NAME}
    PROPERTIES POSITION_INDEPENDENT_CODE ON
)

target_include_directories(${FEATURETRACKERNVXCU_LIBRARY_NAME}
    PRIVATE VisionWorks::nvx
    PUBLIC $<BUILD_INTERFACE:${DEMOS_NVX_PUBLIC_INCLUDE_DIRS}>
    PUBLIC VisionWorks::visionworks
)

target_link_libraries(${FEATURETRACKERNVXCU_LIBRARY_NAME}
    PRIVATE VisionWorks::nvx
    PUBLIC VisionWorks::visionworks
)

target_compile_features(${FEATURETRACKERNVXCU_LIBRARY_NAME} PUBLIC cxx_std_11)
