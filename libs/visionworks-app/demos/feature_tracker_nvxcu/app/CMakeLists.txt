# Copyright (c) 2014-2016, NVIDIA CORPORATION. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#  * Neither the name of NVIDIA CORPORATION nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


set(FEATURETRACKERNVXCU_APP_NAME "nvx_demo_feature_tracker_nvxcu")

set(FEATURETRACKERNVXCU_APP_srcs
    main_feature_tracker_nvxcu.cpp
)

add_executable(${FEATURETRACKERNVXCU_APP_NAME} ${FEATURETRACKERNVXCU_APP_srcs})

target_include_directories(${FEATURETRACKERNVXCU_APP_NAME}
    PUBLIC VisionWorks::feature_tracker_nvxcu
    PUBLIC VisionWorks::nvx
)

target_link_libraries(${FEATURETRACKERNVXCU_APP_NAME}
    PUBLIC VisionWorks::feature_tracker_nvxcu
    PUBLIC VisionWorks::nvx
)

# nvxio/NVX/Application.cpp depends on a specific directory structure
set_target_properties(${FEATURETRACKERNVXCU_APP_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

target_compile_features(${FEATURETRACKERNVXCU_APP_NAME} PUBLIC cxx_std_11)
