# Directories for public includes for each demo's functionality.
set(DEMOS_NVX_PUBLIC_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/demos-includes/NVX/)
set(DEMOS_OVX_PUBLIC_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/demos-includes/OVX/)

add_subdirectory(feature_tracker)
add_subdirectory(feature_tracker_nvxcu)
add_subdirectory(hough_transform)
add_subdirectory(motion_estimation)
add_subdirectory(stereo_matching)
add_subdirectory(video_stabilizer)
