# nvxio/NVX/Application.cpp depends on a specific directory structure
add_custom_target(application-data-directory ALL
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/data ${CMAKE_BINARY_DIR}/data
)

add_subdirectory(nvxio)
add_subdirectory(demos)
add_subdirectory(samples)
