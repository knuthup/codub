/*
# Copyright (c) 2020, Andrew Knuthup
# Copyright (c) 2014-2016, NVIDIA CORPORATION
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <sstream>
#include <stdexcept>

#include <cuda_runtime_api.h>
#include <cuda.h>

#include <VX/vx.h>

#define CUDA_SAFE_CALL(cudaOp) \
    do \
    { \
        cudaError_t err = (cudaOp); \
        if (err != cudaSuccess) \
        { \
            std::ostringstream ostr; \
            ostr << "CUDA Error in " << #cudaOp << __FILE__ << " file " << __LINE__ << " line : " << cudaGetErrorString(err); \
            throw std::runtime_error(ostr.str()); \
        } \
    } while (0)

#define CU_SAFE_CALL(cuOp) \
    do \
    { \
        CUresult result = (cuOp); \
        if (result != CUDA_SUCCESS) \
        { \
            std::ostringstream ostr; \
            const char* strerr = nullptr; \
            cuGetErrorString(result, &strerr); \
            ostr << "CUDA EGL Error in " << #cuOp << __FILE__ << " file " << __LINE__ << " line : " << strerr; \
            throw std::runtime_error(ostr.str()); \
        } \
    } while (0)

#define VX_SAFE_CALL(vxOp) \
    do \
    { \
        vx_status status = (vxOp); \
        if (status != VX_SUCCESS) \
        { \
            std::ostringstream ostr; \
            ostr << #vxOp << " failure [status = " << status << "]" << " in file " << __FILE__ << " line " << __LINE__; \
            throw std::runtime_error(ostr.str()); \
        } \
    } while (0)

#define JETSON_MM_SAFE_CALL(nvOp) \
    do \
    { \
        int ret = (nvOp); \
        if (ret != 0) \
        { \
            std::ostringstream ostr; \
            ostr << "Jetson L4T Multimedia API Error in " << #nvOp << __FILE__ << " file " << __LINE__ << " line"; \
            throw std::runtime_error(ostr.str()); \
        } \
    } while (0)
