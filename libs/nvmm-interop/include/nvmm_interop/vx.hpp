/*
# Copyright (c) 2020, Andrew Knuthup
# Copyright (c) 2014-2016, NVIDIA CORPORATION
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "egl.hpp"
#include "utility.h"

#include <cassert>
#include <sstream>
#include <stdexcept>

#include <gst/gst.h>

#include <VX/vx.h>
#include <NVX/nvx.h>
#include <NVX/nvxcu.h>

#include <cuda_runtime_api.h>


namespace nvmm {

namespace vx {


class Copier
{
    vx_image image;

    vx_enum usage;
    nvxcu_df_image_e format;
    vx_uint32 width;
    vx_uint32 height;

    static constexpr const std::size_t VX_MAX_PLANES = 1;

    struct Plane
    {
        void* data;
        vx_imagepatch_addressing_t addr;
        vx_map_id map_id;
    } planes[VX_MAX_PLANES];

    vx_size num_planes;

  public:
    Copier(vx_image image, vx_enum usage, vx_enum mem_type)
      : image(image)
      , usage(usage)
      , num_planes(0)
    {
        // TODO: Support other formats
        assert(mem_type == NVX_MEMORY_TYPE_CUDA);

        vx_df_image_e vx_format = VX_DF_IMAGE_VIRT;

        VX_SAFE_CALL(vxQueryImage(image, VX_IMAGE_ATTRIBUTE_PLANES, &num_planes, sizeof(num_planes)));
        assert(num_planes == 1);

        VX_SAFE_CALL(vxQueryImage(image, VX_IMAGE_ATTRIBUTE_FORMAT, &vx_format, sizeof(vx_format)));
        VX_SAFE_CALL(vxQueryImage(image, VX_IMAGE_ATTRIBUTE_WIDTH, &width, sizeof(width))); 
        VX_SAFE_CALL(vxQueryImage(image, VX_IMAGE_ATTRIBUTE_HEIGHT, &height, sizeof(height)));

        format = static_cast<nvxcu_df_image_e>(vx_format);

        for (vx_size p = 0; p < num_planes; ++p)
        {
            auto& plane = planes[p];
            VX_SAFE_CALL(
                vxMapImagePatch(
                    image, nullptr, p, &plane.map_id, &plane.addr,
                    &plane.data, usage, mem_type, VX_NOGAP_X
                )
            );
        }
    }

    ~Copier()
    {
        for (vx_size p = 0; p < num_planes; ++p)
        {
            const auto& plane = planes[p];
            vxUnmapImagePatch(image, plane.map_id);
        }
    }

    void copy_to(CUeglFrame& frame) const
    {
        assert(width == frame.width);
        assert(height == frame.height);
        assert(planes[0].addr.stride_x == frame.numChannels);

        const cudaMemcpyKind copy_kind = cudaMemcpyDeviceToDevice;

        void* egl_ptr = frame.frame.pPitch[0];
        const std::size_t egl_pitch = frame.pitch;

        const void* vx_ptr = planes[0].data;
        const std::size_t vx_pitch = planes[0].addr.stride_y;

        CUDA_SAFE_CALL(
            cudaMemcpy2D(
                egl_ptr, egl_pitch,
                vx_ptr, vx_pitch,
                frame.width * frame.numChannels, frame.height,
                copy_kind
            )
        );
    }

    void copy_from(const CUeglFrame& frame)
    {
        assert(width == frame.width);
        assert(height == frame.height);
        assert(planes[0].addr.stride_x == frame.numChannels);

        const cudaMemcpyKind copy_kind = cudaMemcpyDeviceToDevice;

        const void* egl_ptr = frame.frame.pPitch[0];
        const std::size_t egl_pitch = frame.pitch;

        void* vx_ptr = planes[0].data;
        const std::size_t vx_pitch = planes[0].addr.stride_y;

        CUDA_SAFE_CALL(
            cudaMemcpy2D(
                vx_ptr, vx_pitch,
                egl_ptr, egl_pitch,
                frame.width * frame.numChannels, frame.height,
                copy_kind
            )
        );
    }
};


// For mapping and unmapping images directly to and from device memory.
// Set egl.display accordingly if needed.
struct Handle
{
    Handle()
      : image(nullptr)
    {
    }

    nvmm::egl::Handle egl;
    vx_image image;
};


// Read vx_image from buffer
inline void read(vx_image image, GstBuffer& buffer_ref, EGLDisplay display = nullptr)
{
    nvmm::egl::Handle handle;
    handle.display = display;

    nvmm::egl::map<GST_MAP_READ>(handle, buffer_ref);

    // Copy the EGL Frame to the vx_image
    Copier copier(image, VX_WRITE_ONLY, NVX_MEMORY_TYPE_CUDA);
    copier.copy_from(handle.frame);

    nvmm::egl::unmap(handle, buffer_ref);
}


// Write from vx_image to buffer
inline void write(vx_image image, GstBuffer& buffer_ref, EGLDisplay display = nullptr)
{
    nvmm::egl::Handle handle;
    handle.display = display;

    nvmm::egl::map<GST_MAP_WRITE>(handle, buffer_ref);

    // Copy the vx_image to the EGL Frame
    Copier copier(image, VX_READ_ONLY, NVX_MEMORY_TYPE_CUDA);
    copier.copy_to(handle.frame);

    nvmm::egl::unmap(handle, buffer_ref);
}


// Map buffer to vx_image
// unmap must be called after operations on the image are complete
template <GstMapFlags flags = GST_MAP_READWRITE>
inline void map(Handle& handle, vx_context context, GstBuffer& buffer_ref)
{
    auto& image = handle.image;
    auto& frame = handle.egl.frame;

    nvmm::egl::map<flags>(handle.egl, buffer_ref);

    vx_imagepatch_addressing_t addr;
    addr.dim_x = frame.width;
    addr.dim_y = frame.height;
    addr.stride_x = frame.numChannels;
    addr.stride_y = frame.pitch;
    void* ptrs[] = { frame.frame.pPitch[0] };
    image =
        vxCreateImageFromHandle(
            context,
            VX_DF_IMAGE_RGBX,
            &addr,
            ptrs,
            NVX_MEMORY_TYPE_CUDA
        );
}


// Unmap buffer from vx_image
// Use the handle returned from map
inline void unmap(Handle& handle, GstBuffer& buffer_ref)
{
    auto& image = handle.image;
    vxReleaseImage(&image);

    nvmm::egl::unmap(handle.egl, buffer_ref);
};


} // namespace vx {

} // namespace nvmm {
