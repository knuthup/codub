/*
# Copyright (c) 2020, Andrew Knuthup
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "utility.h"

#include <sstream>
#include <stdexcept>

#include <gst/gst.h>

#include <nvbuf_utils.h>
#include <cuda_runtime_api.h>
#include <cudaEGL.h>


namespace nvmm {

namespace egl {


// For mapping and unmapping EGL frames directly to and from device memory.
// Set display accordingly if needed.
struct Handle
{
    Handle()
      : info(GST_MAP_INFO_INIT)
      , display(nullptr)
      , image(nullptr)
      , resource(nullptr)
    {
    }

    GstMapInfo info;
    EGLDisplay display;
    EGLImageKHR image;
    CUgraphicsResource resource;
    CUeglFrame frame;
};


// Reference: https://en.cppreference.com/w/cpp/language/if
// The discarded statement can't be ill-formed for every possible
// specialization
template <GstMapFlags flags> struct always_false : std::false_type {};


template <GstMapFlags flags>
constexpr unsigned int convert_flags_to_cu_graphics() noexcept
{
    if constexpr (flags == GST_MAP_READ)
    {
        return CU_GRAPHICS_MAP_RESOURCE_FLAGS_READ_ONLY;
    }
    else if constexpr (flags == GST_MAP_WRITE)
    {
        return CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD;
    }
    else if constexpr (flags == GST_MAP_READWRITE)
    {
        return CU_GRAPHICS_MAP_RESOURCE_FLAGS_NONE;
    }
    else
    {
        static_assert(always_false<flags>::value, "Invalid GstMapFlags");
    }
}


// Wrap a return-code around NvEGLImageFromFd
inline int nv_egl_image_from_fd_with_rc(EGLImageKHR& image, EGLDisplay display, int dmabuf_fd)
{
    image = NvEGLImageFromFd(display, dmabuf_fd);
    return (image == nullptr);
}


// Copy to or from buffer
template <GstMapFlags gst_flags = GST_MAP_READWRITE>
inline void map(Handle& handle, GstBuffer& buffer_ref)
{
    GstBuffer* buffer = &buffer_ref;

    auto& info = handle.info;
    auto& display = handle.display;
    auto& image = handle.image;
    auto& resource = handle.resource;
    auto& frame = handle.frame;

    // Retrieve the EGLImage from the NVMM file descriptor
    gst_buffer_map(buffer, &info, gst_flags);

    int dmabuf_fd = 0;
    JETSON_MM_SAFE_CALL(ExtractFdFromNvBuffer(info.data, &dmabuf_fd));
    JETSON_MM_SAFE_CALL(
        nv_egl_image_from_fd_with_rc(image, display, dmabuf_fd)
    );

    constexpr unsigned int cu_map_flags =
        convert_flags_to_cu_graphics<gst_flags>();

    // Initialize CUDA context
    CUDA_SAFE_CALL(cudaFree(0));

    // Map the EGL frame
    resource = nullptr;
    CU_SAFE_CALL(cuGraphicsEGLRegisterImage(&resource, image, cu_map_flags));
    CU_SAFE_CALL(cuGraphicsResourceGetMappedEglFrame(&frame, resource, 0, 0));

    CU_SAFE_CALL(cuCtxSynchronize());
}


inline void unmap(Handle& handle, GstBuffer& buffer_ref)
{
    GstBuffer* buffer = &buffer_ref;

    auto& info = handle.info;
    auto& display = handle.display;
    auto& image = handle.image;
    auto& resource = handle.resource;

    // Unmap the EGL frame
    CU_SAFE_CALL(cuCtxSynchronize());
    CU_SAFE_CALL(cuGraphicsUnregisterResource(resource));

    // Destroy the EGLImage and unmap the buffer
    JETSON_MM_SAFE_CALL(NvDestroyEGLImage(display, image));
    gst_buffer_unmap(buffer, &info);
}


} // namespace egl {

} // namespace nvmm {

